package org.softuni.advertwall.integration.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.softuni.advertwall.domain.entity.CategoryEntity;
import org.softuni.advertwall.domain.model.binding.cateogory.CategoryCreateBindingModel;
import org.softuni.advertwall.domain.model.service.CategoryServiceModel;
import org.softuni.advertwall.repository.CategoryRepository;
import org.softuni.advertwall.service.CategoryService;
import org.softuni.advertwall.service.CloudinaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
@RunWith(SpringRunner.class)
public class CategoryServiceTest {
    @Autowired
    private CategoryService service;
    @Autowired
    private ModelMapper modelMapper;

    @MockBean
    private CategoryRepository mockCategoryRepository;
    @MockBean
    private CloudinaryService mockCloudinaryService;

    @Test
    public void createCategory_whenValid_categoryCreated() {
        CategoryCreateBindingModel category = new CategoryCreateBindingModel();

        category.setName("somename");
        when(mockCloudinaryService.uploadImage(any()))
                .thenReturn("fileurl");
        when(mockCategoryRepository.saveAndFlush(any()))
                .thenReturn(new CategoryEntity());

        service.createCategory(this.modelMapper.map(category, CategoryServiceModel.class), null);
        verify(mockCategoryRepository)
                .saveAndFlush(any());
    }


}
