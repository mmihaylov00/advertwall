$(document).ready(function () {
    $("input").blur(function() {
        $(this).addClass('is-invalid');
        var call = postData('http://localhost:8080/api/forms/check-forgotten-password', this.name, "application/json");
        call.then(response => response.json()).then((output) => {
            if (JSON.stringify(output).length === 2){
                $(this).removeClass('is-invalid');
                $(this).next('label').removeClass('text-danger');
                $(this).next('label').next('small').text('');
            }else {
                $(this).addClass('is-invalid');
                $(this).next('label').addClass('text-danger');
                let name = this.name;
                $(this).next('label').next('small').html(output[name]);
            }
        });
    });
});