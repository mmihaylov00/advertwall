function postData(url = ``, data, contentType) {
    if (contentType === null) {
        contentType = "application/json";
    }
    // Default options are marked with *
    return fetch(url, {
        method: "POST", // *GET, POST, PUT, DELETE, etc.
        headers: {
            "Content-Type": contentType,
            // "Content-Type": "application/x-www-form-urlencoded",
        },
        body: JSON.stringify(getFormData($('form'), data)), // body data type must match "Content-Type" header
    }); // parses JSON response into native Javascript objects
}

function getFormData(data, field) {
    let unindexed_array = data.serializeArray();
    let indexed_array = {};

    $.map(unindexed_array, function (n, i) {
        if (field === n['name']) {
            indexed_array[n['name']] = n['value'];
        }
        if (field === 'confirmPassword' && n['name'] === 'password'){
            indexed_array[n['name']] = n['value'];
        }
    });
    return indexed_array;
}