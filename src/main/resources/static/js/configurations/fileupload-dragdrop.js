$(document).ready(function () {


    let $fileuploaderInput = $('input[name="file"]');
    $fileuploaderInput.fileuploader({
        extensions: ['jpg', 'jpeg', 'png'],
        changeInput: '<div class="fileuploader-input">' +
            '<div class="fileuploader-input-inner">' +
            '<i class="fas fa-cloud-upload-alt fileuploader-main-icon"></i>' +
            '<h3 class="fileuploader-input-caption"><span>${captions.feedback}</span></h3>' +
            '<p>${captions.or}</p>' +
            '<div class="fileuploader-input-button"><span>${captions.button}</span></div>' +
            '</div>' +
            '</div>',
        theme: 'dragdrop',
        captions: {
            feedback: 'Drag and drop files here',
            feedback2: 'Drag and drop files here',
            drop: 'Drag and drop files here',
            or: 'or',
            button: 'Browse files',
        },
        enableApi: true,
        addMore: false,
        limit: 1,
        clipboardPaste: true
    });
});