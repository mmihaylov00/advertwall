$(document).ready(function () {


    let $fileuploaderInput = $('input[name="files"]');
    $fileuploaderInput.fileuploader({
        extensions: ['jpg', 'jpeg', 'png'],
        changeInput: ' ',
        inputNameBrackets: false,
        theme: 'thumbnails',
        enableApi: true,
        addMore: true,
        limit: 12,
        thumbnails: {
            box: '<div class="fileuploader-items">' +
                '            <div class="row justify-content-center col-3">' +
                '                <h4 class="text-center text-dark">Thumbnail</h4>' +
                '            </div>' +
                '<ul class="fileuploader-items-list">' +
                '<li class="fileuploader-thumbnails-input"><div class="fileuploader-thumbnails-input-inner"><i>+</i></div></li>' +
                '</ul>' +
                '</div>',
            item: '<li class="fileuploader-item file-has-popup">' +
                '<div class="fileuploader-item-inner">' +
                '<div class="actions-holder">' +
                '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i class="">×</i></a>' +
                '</div>' +
                '<div class="thumbnail-holder">' +
                '${image}' +
                '<span class="fileuploader-action-popup"></span>' +
                '</div>' +
                '</div>' +
                '</li>',
            startImageRenderer: true,
            synchronImages: true,
            canvasImage: false,
            _selectors: {
                list: '.fileuploader-items-list',
                item: '.fileuploader-item',
                start: '.fileuploader-action-start',
                retry: '.fileuploader-action-retry',
                remove: '.fileuploader-action-remove'
            },
            removeConfirmation: false,
            onItemShow: function (item, listEl, parentEl, newInputEl, inputEl) {
                var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                    api = $.fileuploader.getInstance(inputEl.get(0));

                plusInput.insertAfter(item.html)[api.getOptions().limit && api.getChoosedFiles().length >= api.getOptions().limit ? 'hide' : 'show']();

                if (item.format == 'image') {
                    item.html.find('.fileuploader-item-icon').hide();
                }
            }
        },
        skipFileNameCheck: false,
        clipboardPaste: true,
        popup: {
            // popup append to container {String, jQuery Object}
            container: 'body',

            // enable arrows {Boolean}
            arrows: true,

            // loop the arrows {Boolean}
            loop: false,

            // popup HTML {String, Function}
            template: function (data) {
                return '<div class="fileuploader-popup-preview">' +
                    '<a class="fileuploader-popup-move" data-action="prev"></a>' +
                    '<div class="fileuploader-popup-node ${format}">${reader.node}</div>' +
                    '<div class="fileuploader-popup-content">' +
                    '<ul class="fileuploader-popup-tools">' +
                    (data.format == 'image' && data.editor ? (data.editor.cropper ? '<li>' +
                            '<a data-action="crop">' +
                            '<i></i>' +
                            '<span>${captions.crop}</span>' +
                            '</a>' +
                            '</li>' : '') +
                            (data.editor.rotate ? '<li>' +
                                '<a data-action="rotate-cw">' +
                                '<i></i>' +
                                '<span>${captions.rotate}</span>' +
                                '</a>' +
                                '</li>' : '') : ''
                    ) +
                    '<li>' +
                    '<a data-action="remove">' +
                    '<i></i>' +
                    '<span>${captions.remove}</span>' +
                    '</a>' +
                    '</li>' +
                    '</ul>' +
                    '<div class="fileuploader-popup-buttons">' +
                    '<a class="fileuploader-popup-button" data-action="cancel">${captions.cancel}</a>' +
                    '<a class="fileuploader-popup-button button-success" data-action="save">${captions.confirm}</a>' +
                    '</div>' +
                    '</div>' +
                    '<a class="fileuploader-popup-move" data-action="next"></a>' +
                    '</div>';
            },

            // Callback fired after creating the popup
            // we will trigger by default buttons with custom actions
            onShow: function (item) {
                item.popup.html.on('click', '[data-action="prev"]', function (e) {
                    item.popup.move('prev');
                }).on('click', '[data-action="next"]', function (e) {
                    item.popup.move('next');
                }).on('click', '[data-action="crop"]', function (e) {
                    if (item.editor)
                        item.editor.cropper();
                }).on('click', '[data-action="rotate-cw"]', function (e) {
                    if (item.editor)
                        item.editor.rotate();
                }).on('click', '[data-action="remove"]', function (e) {
                    item.popup.close();
                    item.remove();
                }).on('click', '[data-action="cancel"]', function (e) {
                    item.popup.close();
                }).on('click', '[data-action="save"]', function (e) {
                    if (item.editor)
                        item.editor.save();
                    if (item.popup.close)
                        item.popup.close();
                });
            },

            // Callback fired after closing the popup
            onHide: null
        },
        dragDrop: true,
        afterRender: function (listEl, parentEl, newInputEl, inputEl) {
            var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                api = $.fileuploader.getInstance(inputEl.get(0));

            plusInput.on('click', function () {
                api.open();
            });
        },
        onRemove: function (item, listEl, parentEl, newInputEl, inputEl) {
            var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                api = $.fileuploader.getInstance(inputEl.get(0));

            if (api.getOptions().limit && api.getChoosedFiles().length - 1 < api.getOptions().limit)
                plusInput.show();
        },
        editor: {
            cropper: false
        }
    });
});