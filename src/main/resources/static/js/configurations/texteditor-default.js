
$(document).ready(function() {
    $('.editor').summernote({
        height: 350,
        minHeight: 300,
        maxHeight: 500,
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'fontsize', 'clear']],
            ['colors', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['operations', ['undo', 'redo']]
        ],
        placeholder: 'Description',
        dialogsFade: true,
        disableDragAndDrop: true
    });
});