$(".alert").alert('close');

function countChars(val, maxChars, name, labelName) {
    let len = val.value.length;
    if (len >= maxChars) {
        val.value = val.value.substring(0, maxChars);
        len = val.value.length;
    }
    let label = $('#' + labelName);
    label.empty();
    label.append(name +" " + len + "/" + maxChars);

}
