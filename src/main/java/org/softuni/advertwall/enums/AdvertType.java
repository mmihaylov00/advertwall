package org.softuni.advertwall.enums;

public enum AdvertType {
    SELLING,
    BUYING;

    AdvertType() {
    }
}
