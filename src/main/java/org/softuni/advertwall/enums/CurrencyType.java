package org.softuni.advertwall.enums;

public enum CurrencyType {
    BGN,
    EUR,
    USD;

    CurrencyType() {
    }
}
