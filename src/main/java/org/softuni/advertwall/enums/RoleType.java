package org.softuni.advertwall.enums;

public enum RoleType {
    ADMINISTRATOR,
    MODERATOR,
    USER;

    RoleType() {
    }
}
