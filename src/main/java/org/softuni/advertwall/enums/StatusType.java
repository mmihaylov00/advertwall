package org.softuni.advertwall.enums;

public enum StatusType {
    PUBLISHED,
    CLOSED;

    StatusType() {
    }
}
