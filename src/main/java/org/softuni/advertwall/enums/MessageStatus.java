package org.softuni.advertwall.enums;

public enum MessageStatus {
    SEND,
    READ;

    MessageStatus() {
    }

}
