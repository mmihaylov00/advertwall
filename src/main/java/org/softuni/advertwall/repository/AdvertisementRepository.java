package org.softuni.advertwall.repository;


import org.softuni.advertwall.domain.entity.AdvertisementEntity;
import org.softuni.advertwall.enums.AdvertType;
import org.softuni.advertwall.enums.StatusType;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface AdvertisementRepository extends JpaRepository<AdvertisementEntity, String> {

    @Query("SELECT e from AdvertisementEntity e where e.creator.id = :id")
    List<AdvertisementEntity> getAllByUserId(String id);

    List<AdvertisementEntity> findAllByAdvertTypeAndStatusOrderByCreationDateDesc(AdvertType advertType, StatusType status, Pageable pageable);
    Integer countAllByAdvertType(AdvertType advertType);

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM user_followed_ads WHERE ad_id = :adId AND user_id = :userId", nativeQuery = true)
    void unfollowAd(@Param("userId") String userId, @Param("adId") String adId);

    @Modifying
    @Transactional
    @Query(value = "UPDATE AdvertisementEntity e SET e.views = e.views + 1 WHERE e.id = :id")
    void addView(String id);
}
