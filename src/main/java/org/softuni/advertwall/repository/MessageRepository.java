package org.softuni.advertwall.repository;

import org.softuni.advertwall.domain.entity.AdvertisementEntity;
import org.softuni.advertwall.domain.entity.PrivateMessageEntity;
import org.softuni.advertwall.domain.entity.UserEntity;
import org.softuni.advertwall.enums.MessageStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<PrivateMessageEntity, String> {

    @Modifying
    @Transactional
    @Query("UPDATE PrivateMessageEntity e SET e.status = :read WHERE " +
            "e.advert = :advert AND e.sender = :sender AND e.receiver = :receiver")
    void readMessages(UserEntity sender, UserEntity receiver, AdvertisementEntity advert, MessageStatus read);

    @Query("SELECT e FROM PrivateMessageEntity e WHERE " +
            "e.advert = :advert AND " +
            "((e.receiver = :first AND e.sender = :second) OR" +
            "(e.sender = :first AND e.receiver = :second)) " +
            "ORDER BY e.timestamp DESC")
    List<PrivateMessageEntity> findAllByAdvertBetweenUsers(AdvertisementEntity advert, UserEntity first, UserEntity second);

    @Query("SELECT e FROM PrivateMessageEntity e WHERE " +
            "e.receiver = :user OR e.sender = :user " +
            "ORDER BY e.timestamp DESC")
    List<PrivateMessageEntity> findAllConversations(UserEntity user);
}
