package org.softuni.advertwall.repository;

import org.softuni.advertwall.domain.entity.RoleEntity;
import org.softuni.advertwall.enums.RoleType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<RoleEntity, Integer> {
    RoleEntity getFirstByRoleName(RoleType role);
}
