package org.softuni.advertwall.repository;

import org.softuni.advertwall.domain.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, String> {
    UserEntity findByEmail(String email);

    UserEntity findFirstById(String id);

    UserEntity findFirstByConfirmationCode(String code);

    @Modifying
    @Transactional
    @Query("UPDATE UserEntity e SET e.hasNotification = :hasNotification WHERE e = :user")
    void setNotification(UserEntity user, Boolean hasNotification);
}
