package org.softuni.advertwall.domain.entity;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "users")
public class UserEntity implements UserDetails {
    @Id
    @GeneratedValue(generator = "uuid-string")
    @GenericGenerator(name = "uuid-string",
            strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", nullable = false, unique = true, updatable = false, length = 36)
    private String id;

    @Column(name = "email", unique = true, nullable = false, length = 40)
    private String email;

    @Column(name = "password", length = 128)
    private String password;

    @Column(name = "first_name", length = 20)
    private String firstName;

    @Column(name = "last_name", length = 20)
    private String lastName;

    @Column(name = "city", length = 128)
    private String city;

    @Column(name = "tel_num", length = 16)
    private String telNum;

    @Column(name = "confirmation_code", length = 36)
    private String confirmationCode;

    @Column(name = "receive_emails", columnDefinition="tinyint(1) default 1")
    private Boolean receiveEmails;

    @Column(name = "enabled", columnDefinition="tinyint(1) default 0")
    private Boolean isEnabled;

    @Column(name = "notification", columnDefinition="tinyint(1) default 0")
    private Boolean hasNotification;

    @ManyToOne
    @JoinColumn(name = "role_id")
    private RoleEntity role;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "creator")
    private List<AdvertisementEntity> createdAdverts;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sender")
    private List<PrivateMessageEntity> messagesSent;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "receiver")
    private List<PrivateMessageEntity> messagesReceived;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "user_followed_ads",
            joinColumns = { @JoinColumn(name = "user_id") },
            inverseJoinColumns = { @JoinColumn(name = "ad_id") }
    )
    private Set<AdvertisementEntity> followedAdverts;

    public UserEntity() {
        this.createdAdverts = new ArrayList<>();
        this.followedAdverts = new HashSet<>();
        this.messagesReceived = new ArrayList<>();
        this.messagesSent = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getReceiveEmails() {
        return receiveEmails;
    }

    public void setReceiveEmails(Boolean receiveEmails) {
        this.receiveEmails = receiveEmails;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Set<RoleEntity> roleEntities = new HashSet<>();
        roleEntities.add(this.role);
        return roleEntities;
    }

    public Set<AdvertisementEntity> getFollowedAdverts() {
        return followedAdverts;
    }

    public void setFollowedAdverts(Set<AdvertisementEntity> followedAdverts) {
        this.followedAdverts = followedAdverts;
    }

    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.firstName + " " + this.lastName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return isEnabled;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTelNum() {
        return telNum;
    }

    public void setTelNum(String telNum) {
        this.telNum = telNum;
    }

    public RoleEntity getRole() {
        return role;
    }

    public void setRole(RoleEntity role) {
        this.role = role;
    }

    public Boolean getEnabled() {
        return isEnabled;
    }

    public List<AdvertisementEntity> getCreatedAdverts() {
        return createdAdverts;
    }

    public void setCreatedAdverts(List<AdvertisementEntity> createdAdverts) {
        this.createdAdverts = createdAdverts;
    }

    public String getConfirmationCode() {
        return confirmationCode;
    }

    public void setConfirmationCode(String confirmationCode) {
        this.confirmationCode = confirmationCode;
    }

    public void setEnabled(Boolean enabled) {
        isEnabled = enabled;
    }

    public List<PrivateMessageEntity> getMessagesReceived() {
        return messagesReceived;
    }

    public void setMessagesReceived(List<PrivateMessageEntity> messagesReceived) {
        this.messagesReceived = messagesReceived;
    }

    public List<PrivateMessageEntity> getMessagesSent() {
        return messagesSent;
    }

    public void setMessagesSent(List<PrivateMessageEntity> messagesSent) {
        this.messagesSent = messagesSent;
    }

    public Boolean getHasNotification() {
        return hasNotification;
    }

    public void setHasNotification(Boolean hasNotification) {
        this.hasNotification = hasNotification;
    }
}
