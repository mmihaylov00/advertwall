package org.softuni.advertwall.domain.entity;

import org.softuni.advertwall.enums.RoleType;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "roles")
public class RoleEntity implements GrantedAuthority {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column(name = "role_name", unique = true)
    private RoleType roleName;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "role")
    private List<UserEntity> users;

    public RoleEntity() {
        this.users = new ArrayList<>();
    }

    public RoleEntity(RoleType roleName) {
        this();
        this.roleName = roleName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public RoleType getRoleName() {
        return roleName;
    }

    public void setRoleName(RoleType roleName) {
        this.roleName = roleName;
    }

    public List<UserEntity> getUsers() {
        return users;
    }

    public void setUsers(List<UserEntity> users) {
        this.users = users;
    }

    @Override
    public String getAuthority() {
        return roleName.name();
    }
}
