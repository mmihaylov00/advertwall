package org.softuni.advertwall.domain.entity;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.softuni.advertwall.enums.MessageStatus;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "private_messages")
public class PrivateMessageEntity {

    @Id
    @GeneratedValue(generator = "uuid-string")
    @GenericGenerator(name = "uuid-string",
            strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", nullable = false, unique = true, updatable = false, length = 36)
    private String id;

    @Column(name = "message", nullable = false)
    @Lob
    private String message;

    @ManyToOne
    @JoinColumn(name = "sender")
    private UserEntity sender;

    @ManyToOne
    @JoinColumn(name = "receiver")
    private UserEntity receiver;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private MessageStatus status;

    @ManyToOne
    @JoinColumn(name = "advert_id")
    private AdvertisementEntity advert;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "timestamp")
    private Date timestamp;

    public PrivateMessageEntity() {
        this.timestamp = new Date();
        this.status = MessageStatus.SEND;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserEntity getSender() {
        return sender;
    }

    public void setSender(UserEntity sender) {
        this.sender = sender;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public UserEntity getReceiver() {
        return receiver;
    }

    public void setReceiver(UserEntity receiver) {
        this.receiver = receiver;
    }

    public MessageStatus getStatus() {
        return status;
    }

    public void setStatus(MessageStatus status) {
        this.status = status;
    }

    public AdvertisementEntity getAdvert() {
        return advert;
    }

    public void setAdvert(AdvertisementEntity advert) {
        this.advert = advert;
    }
}
