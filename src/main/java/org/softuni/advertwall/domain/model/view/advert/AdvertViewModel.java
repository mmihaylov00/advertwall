package org.softuni.advertwall.domain.model.view.advert;

import org.softuni.advertwall.enums.AdvertType;
import org.softuni.advertwall.enums.CurrencyType;
import org.softuni.advertwall.enums.StatusType;

import java.math.BigDecimal;
import java.util.List;

public class AdvertViewModel {
    private String id;
    private String title;
    private String description;
    private AdvertType advertType;
    private StatusType status;
    private CurrencyType currency;
    private Boolean negotiable;
    private BigDecimal price;
    private String categoryName;
    private String creatorUsername;
    private String creatorId;
    private String creatorCity;
    private String creatorTelNum;
    private Boolean isOwner;
    private Boolean isFollowed;
    private Boolean updated;
    private List<String> images;

    public AdvertViewModel() {
        isOwner = false;
        isFollowed = false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public AdvertType getAdvertType() {
        return advertType;
    }

    public void setAdvertType(AdvertType advertType) {
        this.advertType = advertType;
    }

    public CurrencyType getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyType currency) {
        this.currency = currency;
    }

    public Boolean getNegotiable() {
        return negotiable;
    }

    public void setNegotiable(Boolean negotiable) {
        this.negotiable = negotiable;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCreatorUsername() {
        return creatorUsername;
    }

    public void setCreatorUsername(String creatorUsername) {
        this.creatorUsername = creatorUsername;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreatorCity() {
        return creatorCity;
    }

    public void setCreatorCity(String creatorCity) {
        this.creatorCity = creatorCity;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public Boolean getOwner() {
        return isOwner;
    }

    public void setOwner(Boolean owner) {
        isOwner = owner;
    }

    public String getCreatorTelNum() {
        return creatorTelNum;
    }

    public void setCreatorTelNum(String creatorTelNum) {
        this.creatorTelNum = creatorTelNum;
    }

    public StatusType getStatus() {
        return status;
    }

    public void setStatus(StatusType status) {
        this.status = status;
    }

    public Boolean getFollowed() {
        return isFollowed;
    }

    public void setFollowed(Boolean followed) {
        isFollowed = followed;
    }

    public Boolean getUpdated() {
        return updated;
    }

    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }
}
