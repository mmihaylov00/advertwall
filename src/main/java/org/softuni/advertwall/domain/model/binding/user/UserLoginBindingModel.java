package org.softuni.advertwall.domain.model.binding.user;

import javax.validation.constraints.NotBlank;

public class UserLoginBindingModel {
    @NotBlank
    private String email;
    @NotBlank
    private String password;

    public UserLoginBindingModel() {
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
