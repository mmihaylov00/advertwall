package org.softuni.advertwall.domain.model.service;

import org.softuni.advertwall.enums.RoleType;

import java.util.Date;

public class UserServiceModel {
    private String id;
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String city;
    private String telNum;
    private RoleType role;
    private Boolean receiveEmails;
    private Date latestEmailTime;

    public UserServiceModel() {
    }

    public Date getLatestEmailTime() {
        return latestEmailTime;
    }

    public void setLatestEmailTime(Date latestEmailTime) {
        this.latestEmailTime = latestEmailTime;
    }

    public Boolean getReceiveEmails() {
        return receiveEmails;
    }

    public void setReceiveEmails(Boolean receiveEmails) {
        this.receiveEmails = receiveEmails;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTelNum() {
        return telNum;
    }

    public void setTelNum(String telNum) {
        this.telNum = telNum;
    }

    public RoleType getRole() {
        return role;
    }

    public void setRole(RoleType role) {
        this.role = role;
    }
}
