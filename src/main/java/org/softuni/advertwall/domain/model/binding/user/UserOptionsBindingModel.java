package org.softuni.advertwall.domain.model.binding.user;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UserOptionsBindingModel {
    @Email
    @NotBlank
    private String email;
    @Size(max = 20)
    @NotBlank
    @Pattern(regexp = "^([A-Z][a-z]+(-[A-Z][a-z]+)?)|([А-Я][а-я]+(-[А-Я][а-я]+)?)$|[[:blank:]]*")
    private String firstName;
    @Size(max = 20)
    @NotBlank
    @Pattern(regexp = "^([A-Z][a-z]+(-[A-Z][a-z]+)?)|([А-Я][а-я]+(-[А-Я][а-я]+)?)$|[[:blank:]]*")
    private String lastName;
    @Size(max = 128)
    @NotBlank
    private String city;
    @Size(max = 15)
    @NotBlank
    @Pattern(regexp = "^(\\+)?(359|0)8[789]\\d{1}(|-| )\\d{3}(|-| )\\d{3}$|[[:blank:]]*")
    private String telNum;
    private String oldPassword;
    private String password;
    private String confirmPassword;
    private Boolean receiveEmails;

    public UserOptionsBindingModel() {
    }

    public Boolean getReceiveEmails() {
        return receiveEmails;
    }

    public void setReceiveEmails(Boolean receiveEmails) {
        this.receiveEmails = receiveEmails;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTelNum() {
        return telNum;
    }

    public void setTelNum(String telNum) {
        this.telNum = telNum;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }
}
