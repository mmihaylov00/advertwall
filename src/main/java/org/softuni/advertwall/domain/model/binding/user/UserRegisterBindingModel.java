package org.softuni.advertwall.domain.model.binding.user;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UserRegisterBindingModel {
    @Email
    @NotBlank
    private String email;
    @NotBlank
    @Size(max = 20)
    @Pattern(regexp = "^([A-Z][a-z]+(-[A-Z][a-z]+)?)|([А-Я][а-я]+(-[А-Я][а-я]+)?)$|[[:blank:]]*")
    private String firstName;
    @NotBlank
    @Size(max = 20)
    @Pattern(regexp = "^([A-Z][a-z]+(-[A-Z][a-z]+)?)|([А-Я][а-я]+(-[А-Я][а-я]+)?)$|[[:blank:]]*")
    private String lastName;
    @NotBlank
    @Size(max = 128)
    private String city;
    @NotBlank
    @Size(max = 15)
    @Pattern(regexp = "^(\\+)?(359|0)8[789]\\d{1}(|-| )\\d{3}(|-| )\\d{3}$|[[:blank:]]*")
    private String telNum;
    private Boolean isSend;
    private Boolean receiveEmails;

    public UserRegisterBindingModel() {
        this.isSend = false;
    }
    public UserRegisterBindingModel(Boolean isSend) {
        this.isSend = isSend;
    }

    public Boolean getReceiveEmails() {
        return receiveEmails;
    }

    public void setReceiveEmails(Boolean receiveEmails) {
        this.receiveEmails = receiveEmails;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTelNum() {
        return telNum;
    }

    public void setTelNum(String telNum) {
        this.telNum = telNum;
    }

    public Boolean getSend() {
        return isSend;
    }

    public void setSend(Boolean send) {
        isSend = send;
    }
}
