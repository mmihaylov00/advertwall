package org.softuni.advertwall.domain.model.binding.cateogory;

import org.springframework.web.multipart.MultipartFile;

public class CategoryCreateBindingModel {
    private String name;
    private MultipartFile file;

    public CategoryCreateBindingModel() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
}
