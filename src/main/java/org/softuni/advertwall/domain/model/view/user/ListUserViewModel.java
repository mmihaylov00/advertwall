package org.softuni.advertwall.domain.model.view.user;

import org.softuni.advertwall.enums.RoleType;

public class ListUserViewModel {
    private String id;
    private String email;
    private RoleType role;

    public ListUserViewModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public RoleType getRole() {
        return role;
    }

    public void setRole(RoleType role) {
        this.role = role;
    }
}
