package org.softuni.advertwall.domain.model.binding.message;

public class SendMessageBindingModel{
    private String message;
    private String receiverId;
    private String advertId;

    public SendMessageBindingModel() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getAdvertId() {
        return advertId;
    }

    public void setAdvertId(String advertId) {
        this.advertId = advertId;
    }
}
