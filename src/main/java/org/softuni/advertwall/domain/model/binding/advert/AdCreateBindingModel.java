package org.softuni.advertwall.domain.model.binding.advert;

import org.softuni.advertwall.domain.model.view.category.CategoryViewModel;
import org.softuni.advertwall.enums.AdvertType;
import org.softuni.advertwall.enums.CurrencyType;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class AdCreateBindingModel {
    @NotBlank
    @Size(max = 70)
    private String title;
    @NotBlank
    @Size(max = 8000)
    private String description;
    private BigDecimal price;
    private Boolean negotiable;
    private AdvertType advertType;
    private CurrencyType currency;
    private MultipartFile[] files;
    @NotBlank
    private String categoryName;
    private List<CategoryViewModel> categories;

    public AdCreateBindingModel() {
        this.currency = CurrencyType.BGN;
        this.advertType = AdvertType.SELLING;
        this.categories = new ArrayList<>();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public MultipartFile[] getFiles() {
        return files;
    }

    public void setFiles(MultipartFile[] files) {
        this.files = files;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Boolean getNegotiable() {
        return negotiable;
    }

    public void setNegotiable(Boolean negotiable) {
        this.negotiable = negotiable;
    }

    public CurrencyType getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyType currency) {
        this.currency = currency;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public AdvertType getAdvertType() {
        return advertType;
    }

    public void setAdvertType(AdvertType advertType) {
        this.advertType = advertType;
    }

    public List<CategoryViewModel> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoryViewModel> categories) {
        this.categories = categories;
    }
}
