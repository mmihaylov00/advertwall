package org.softuni.advertwall.domain.model.binding.user;

import javax.validation.constraints.NotBlank;

public class UserEmailBindingModel {
    @NotBlank
    private String email;
    private Boolean isSend;

    public UserEmailBindingModel() {
        this.isSend = false;
    }

    public Boolean getSend() {
        return isSend;
    }

    public void setSend(Boolean send) {
        isSend = send;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
