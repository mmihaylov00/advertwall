package org.softuni.advertwall.domain.model.binding.user;

import javax.validation.constraints.NotBlank;

public class UserUpdatePasswordBindingModel {
    @NotBlank
    private String password;
    @NotBlank
    private String confirmPassword;
    private String token;

    public UserUpdatePasswordBindingModel() {
    }

    public UserUpdatePasswordBindingModel(String token) {
        this.token = token;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
