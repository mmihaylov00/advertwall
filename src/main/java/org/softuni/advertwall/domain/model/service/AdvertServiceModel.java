package org.softuni.advertwall.domain.model.service;

import org.softuni.advertwall.enums.AdvertType;
import org.softuni.advertwall.enums.CurrencyType;
import org.softuni.advertwall.enums.StatusType;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class AdvertServiceModel {
    private String id;
    private String title;
    private String description;
    private BigDecimal price;
    private Boolean negotiable;
    private AdvertType advertType;
    private CurrencyType currency;
    private StatusType status;
    private String categoryName;
    private List<String> images;
    private String creatorId;
    private String creatorUsername;
    private String creatorTelNum;
    private String creatorCity;
    private Boolean isOwner;
    private Boolean isFollowed;

    public AdvertServiceModel() {
        this.images = new ArrayList<>();
    }

    public AdvertServiceModel(List<String> images, String creatorId) {
        this.images = images;
        this.creatorId = creatorId;
    }

    public String getCreatorUsername() {
        return creatorUsername;
    }

    public void setCreatorUsername(String creatorUsername) {
        this.creatorUsername = creatorUsername;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatorCity() {
        return creatorCity;
    }

    public void setCreatorCity(String creatorCity) {
        this.creatorCity = creatorCity;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Boolean getNegotiable() {
        return negotiable;
    }

    public void setNegotiable(Boolean negotiable) {
        this.negotiable = negotiable;
    }

    public AdvertType getAdvertType() {
        return advertType;
    }

    public void setAdvertType(AdvertType advertType) {
        this.advertType = advertType;
    }

    public CurrencyType getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyType currency) {
        this.currency = currency;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreatorTelNum() {
        return creatorTelNum;
    }

    public void setCreatorTelNum(String creatorTelNum) {
        this.creatorTelNum = creatorTelNum;
    }

    public StatusType getStatus() {
        return status;
    }

    public void setStatus(StatusType status) {
        this.status = status;
    }

    public Boolean getOwner() {
        return isOwner;
    }

    public void setOwner(Boolean owner) {
        isOwner = owner;
    }

    public Boolean getFollowed() {
        return isFollowed;
    }

    public void setFollowed(Boolean followed) {
        isFollowed = followed;
    }
}
