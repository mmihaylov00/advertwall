
package org.softuni.advertwall.service;

import org.softuni.advertwall.domain.model.binding.user.UserUpdatePasswordBindingModel;
import org.softuni.advertwall.domain.model.service.UserServiceModel;
import org.softuni.advertwall.enums.RoleType;

import java.util.List;

public interface UserService {
    UserServiceModel addUser(UserServiceModel user);

    UserServiceModel editUser(UserServiceModel user);

    UserServiceModel findByEmail(String id);

    List<UserServiceModel> getAllUsers();

    boolean isRegistered(String email);

    void updatePassword(UserUpdatePasswordBindingModel user);

    void sendForgottenMail(String email);

    UserServiceModel getUser(String id);

    void updateRole(String userId, RoleType role);

    void unsubscribe(String userId);
}
