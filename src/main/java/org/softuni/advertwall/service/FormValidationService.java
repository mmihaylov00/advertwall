package org.softuni.advertwall.service;

import org.softuni.advertwall.domain.model.binding.user.*;

import java.util.Map;

public interface FormValidationService {
    Map<String, String> checkRegistrationForm(UserRegisterBindingModel user, boolean rawMessages);
    Map<String, String> checkLoginForm(UserLoginBindingModel user, boolean rawMessages);
    Map<String, String> checkForgottenPasswordForm(UserEmailBindingModel user, boolean rawMessages);
    Map<String, String> checkUpdatePasswordForm(UserUpdatePasswordBindingModel user, boolean rawMessages);
    Map<String, String> checkOptionsForm(UserOptionsBindingModel user, boolean rawMessages);
}
