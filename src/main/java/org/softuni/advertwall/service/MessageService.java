package org.softuni.advertwall.service;

import org.softuni.advertwall.domain.model.service.ConversationServiceModel;
import org.softuni.advertwall.domain.model.service.MessageServiceModel;

import java.util.List;

public interface MessageService {
    void sendMessage(MessageServiceModel model);

    void readMessages(String senderId, String advertId);

    List<MessageServiceModel> getConversation(String userId, String advertId);

    List<ConversationServiceModel> getConversations();
}
