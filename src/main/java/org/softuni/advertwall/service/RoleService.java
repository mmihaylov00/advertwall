package org.softuni.advertwall.service;

import org.softuni.advertwall.domain.model.service.RoleServiceModel;

import java.util.List;

public interface RoleService {
    List<RoleServiceModel> getAllRoles();

    void generateDBRoles();
}
