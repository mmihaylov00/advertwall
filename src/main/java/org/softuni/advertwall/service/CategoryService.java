package org.softuni.advertwall.service;


import org.softuni.advertwall.domain.model.service.CategoryServiceModel;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface CategoryService {
    List<CategoryServiceModel> getAllCategories();

    CategoryServiceModel createCategory(CategoryServiceModel categoryModel, MultipartFile file);

    CategoryServiceModel getCategoryById(String id);

    CategoryServiceModel editCategory(CategoryServiceModel model, MultipartFile file);
}
