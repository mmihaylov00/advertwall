package org.softuni.advertwall.service;


import org.softuni.advertwall.domain.model.service.AdvertServiceModel;
import org.softuni.advertwall.enums.AdvertType;
import org.softuni.advertwall.enums.StatusType;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

public interface AdvertisementService {
    ArrayList<String> upload(MultipartFile[] files);

    String create(AdvertServiceModel serviceModel);

    List<AdvertServiceModel> getOwn();

    Long getViewsCount(String userId);

    List<AdvertServiceModel> getFollowed();

    void changeFavourite(String adId);

    AdvertServiceModel getById(String id);

    List<AdvertServiceModel> getAdverts(AdvertType type, Integer page);

    Integer getPages(AdvertType advertType);

    boolean isOwner(String advertId);

    void changeStatus(String advertId, StatusType status);
}
