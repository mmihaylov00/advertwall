package org.softuni.advertwall.service.impl;

import org.hibernate.exception.ConstraintViolationException;
import org.modelmapper.MappingException;
import org.modelmapper.ModelMapper;
import org.softuni.advertwall.domain.entity.CategoryEntity;
import org.softuni.advertwall.domain.model.service.CategoryServiceModel;
import org.softuni.advertwall.repository.CategoryRepository;
import org.softuni.advertwall.service.CategoryService;
import org.softuni.advertwall.service.CloudinaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.stream.Collectors;

@Service("categoryService")
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository categoryRepository;
    private final ModelMapper modelMapper;
    private final CloudinaryService cloudinaryService;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository, ModelMapper modelMapper, CloudinaryService cloudinaryService) {
        this.categoryRepository = categoryRepository;
        this.modelMapper = modelMapper;
        this.cloudinaryService = cloudinaryService;
    }


    @Override
    public List<CategoryServiceModel> getAllCategories() {
        return this.categoryRepository.findAll().stream()
                        .map(c -> modelMapper.map(c, CategoryServiceModel.class)).collect(Collectors.toList());
    }

    @Override
    public CategoryServiceModel createCategory(CategoryServiceModel categoryModel, MultipartFile file) {
        String url = this.cloudinaryService.uploadImage(file);
        CategoryEntity entity = modelMapper.map(categoryModel, CategoryEntity.class);
        entity.setImageUrl(url);
        try {
            this.categoryRepository.saveAndFlush(entity);
        }catch (ConstraintViolationException e){
            return null;
        }
        return this.modelMapper.map(entity, CategoryServiceModel.class);
    }

    @Override
    public CategoryServiceModel getCategoryById(String id) {
        try {
            return modelMapper.map(this.categoryRepository.getOne(id), CategoryServiceModel.class);
        }catch (MappingException e){
            return null;
        }
    }

    @Override
    public CategoryServiceModel editCategory(CategoryServiceModel model, MultipartFile file) {
        CategoryEntity entity = this.categoryRepository.getOne(model.getId());
        this.modelMapper.map(model, entity);
        if (file != null) {
            String url = this.cloudinaryService.uploadImage(file);
            entity.setImageUrl(url);
        }

        try {
            this.categoryRepository.saveAndFlush(entity);
        }catch (ConstraintViolationException e){
            return null;
        }

        return this.modelMapper.map(entity, CategoryServiceModel.class);
    }


}
