package org.softuni.advertwall.service.impl;

import org.modelmapper.ModelMapper;
import org.softuni.advertwall.domain.entity.AdvertisementEntity;
import org.softuni.advertwall.domain.entity.UserEntity;
import org.softuni.advertwall.domain.model.service.AdvertServiceModel;
import org.softuni.advertwall.enums.AdvertType;
import org.softuni.advertwall.enums.StatusType;
import org.softuni.advertwall.repository.AdvertisementRepository;
import org.softuni.advertwall.repository.CategoryRepository;
import org.softuni.advertwall.repository.UserRepository;
import org.softuni.advertwall.service.AdvertisementService;
import org.softuni.advertwall.service.CloudinaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("advertisementService")
public class AdvertisementServiceImpl implements AdvertisementService {
    private final AdvertisementRepository advertisementRepository;
    private final CategoryRepository categoryRepository;
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;
    private final CloudinaryService cloudinaryService;

    @Autowired
    public AdvertisementServiceImpl(AdvertisementRepository advertisementRepository, CategoryRepository categoryRepository, UserRepository userRepository, ModelMapper modelMapper, CloudinaryService cloudinaryService) {
        this.advertisementRepository = advertisementRepository;
        this.categoryRepository = categoryRepository;
        this.userRepository = userRepository;
        this.modelMapper = modelMapper;
        this.cloudinaryService = cloudinaryService;
    }


    @Override
    public ArrayList<String> upload(MultipartFile[] files) {
        ArrayList<String> urls = new ArrayList<>();
        for (MultipartFile file : files) {
            try {
                if (file.getBytes().length == 0) {
                    continue;
                }
                String url = this.cloudinaryService.uploadImage(file);
                urls.add(url);
            } catch (IOException ignored) {
            }
        }
        return urls;
    }

    @Override
    public String create(AdvertServiceModel serviceModel) {
        if (serviceModel.getNegotiable() == null) {
            serviceModel.setNegotiable(false);
        }
        AdvertisementEntity entity = this.modelMapper.map(serviceModel, AdvertisementEntity.class);
        entity.setCategory(this.categoryRepository.getOne(serviceModel.getCategoryName()));
        entity.setCreator(this.userRepository.getOne(serviceModel.getCreatorId()));
        entity.setStatus(StatusType.PUBLISHED);

        entity = this.advertisementRepository.saveAndFlush(entity);
        return entity.getId();
    }

    @Override
    public List<AdvertServiceModel> getOwn() {
        UserEntity user = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return this.advertisementRepository.getAllByUserId(user.getId()).stream()
                .map(e -> this.modelMapper.map(e, AdvertServiceModel.class)).collect(Collectors.toList());
    }

    @Override
    public Long getViewsCount(String userId) {
        List<AdvertisementEntity> ads = this.advertisementRepository.getAllByUserId(userId);
        Long sum = 0L;
        for (AdvertisementEntity e : ads) {
            if (e.getViews() != null){
                sum += e.getViews();
            }
            e.setViews(0L);
        }
        this.advertisementRepository.saveAll(ads);
        return sum;
    }

    @Override
    public List<AdvertServiceModel> getFollowed() {
        UserEntity user = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<AdvertServiceModel> output = this.userRepository.getOne(user.getId()).getFollowedAdverts().stream()
                .map(e -> this.modelMapper.map(e, AdvertServiceModel.class)).collect(Collectors.toList());
        return output.stream().filter(e -> e.getStatus() == StatusType.PUBLISHED).collect(Collectors.toList());
    }

    @Override
    public void changeFavourite(String adId) {
        UserEntity u = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AdvertisementEntity ad = this.advertisementRepository.getOne(adId);

        List<String> ids = ad.getFollowers().stream().map(UserEntity::getId).collect(Collectors.toList());

        if (ids.contains(u.getId())) {
            this.advertisementRepository.unfollowAd(u.getId(), ad.getId());
        } else {
            ad.getFollowers().add(u);
            this.advertisementRepository.saveAndFlush(ad);
        }

    }

    @Override
    public AdvertServiceModel getById(String id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        UserEntity user = null;
        if (!authentication.getAuthorities().iterator().next().getAuthority().equals("ROLE_ANONYMOUS")) {
            user = (UserEntity) authentication.getPrincipal();
        }
        AdvertisementEntity entity = this.advertisementRepository.getOne(id);
        if (entity == null) {
            return null;
        }
        AdvertServiceModel output = this.modelMapper.map(entity, AdvertServiceModel.class);
        if (user != null) {
            if (output.getCreatorId().equals(user.getId())) {
                output.setOwner(true);
            } else {
                this.advertisementRepository.addView(id);
            }
            if (entity.getFollowers() != null && !entity.getFollowers().isEmpty()) {
                for (UserEntity follower : entity.getFollowers()) {
                    if (follower.getId().equals(user.getId())) {
                        output.setFollowed(true);
                        break;
                    }
                }
            }
        }
        return output;
    }

    @Override
    public List<AdvertServiceModel> getAdverts(AdvertType type, Integer page) {
        return this.advertisementRepository.findAllByAdvertTypeAndStatusOrderByCreationDateDesc(type, StatusType.PUBLISHED, PageRequest.of(page, 16))
                .stream().map(e -> this.modelMapper.map(e, AdvertServiceModel.class)).collect(Collectors.toList());
    }

    @Override
    public Integer getPages(AdvertType advertType) {
        return this.advertisementRepository.countAllByAdvertType(advertType);
    }

    @Override
    public boolean isOwner(String advertId) {
        UserEntity user = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AdvertisementEntity ad = this.advertisementRepository.getOne(advertId);
        if (ad == null || ad.getId() == null) {
            return false;
        }
        return ad.getCreator().getId().equals(user.getId());
    }

    @Override
    public void changeStatus(String advertId, StatusType status) {
        AdvertisementEntity ad = this.advertisementRepository.getOne(advertId);
        ad.setStatus(status);
        this.advertisementRepository.saveAndFlush(ad);
    }
}
