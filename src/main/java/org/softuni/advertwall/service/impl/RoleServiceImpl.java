package org.softuni.advertwall.service.impl;

import org.hibernate.exception.ConstraintViolationException;
import org.softuni.advertwall.domain.entity.RoleEntity;
import org.softuni.advertwall.domain.model.service.RoleServiceModel;
import org.softuni.advertwall.enums.RoleType;
import org.softuni.advertwall.repository.RoleRepository;
import org.softuni.advertwall.service.RoleService;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("roleService")
public class RoleServiceImpl implements RoleService {
    private final RoleRepository roleRepository;

    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public List<RoleServiceModel> getAllRoles() {
        return null;
    }

    @Override
    public void generateDBRoles() {
        for (RoleType value : RoleType.values()) {
            try {
                this.roleRepository.saveAndFlush(new RoleEntity(value));
            }catch (DataIntegrityViolationException | ConstraintViolationException ignored){

            }
        }
    }
}
