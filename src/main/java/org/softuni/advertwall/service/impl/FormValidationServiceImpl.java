package org.softuni.advertwall.service.impl;

import org.softuni.advertwall.domain.model.binding.user.*;
import org.softuni.advertwall.service.FormValidationService;
import org.softuni.advertwall.service.UserService;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service("formValidationService")
public class FormValidationServiceImpl implements FormValidationService {

    private final UserService userService;
    private final MessageSource messageSource;

    public FormValidationServiceImpl(UserService userService, MessageSource messageSource) {
        this.userService = userService;
        this.messageSource = messageSource;
    }

    @Override
    public Map<String, String> checkRegistrationForm(UserRegisterBindingModel user, boolean rawMessages) {
        Map<String, String> output = new HashMap<>();

        if (user.getEmail() != null) {
            String value = checkField(user.getEmail(), ".+@.+\\..+", "Email.user.email", 0, 0, null);
            if (value != null) {
                output.put("email", value);
            }
            if (user.getEmail() != null && this.userService.isRegistered(user.getEmail())) {
                output.put("email", "email.registered");
            }
        }
        if (user.getFirstName() != null) {
            String value = checkField(user.getFirstName(), "^([A-Z][a-z]+(-[A-Z][a-z]+)?)|([А-Я][а-я]+(-[А-Я][а-я]+)?)$|[[:blank:]]*",
                    "Pattern.user.firstName", 0, 20, "Size.user.firstName");
            if (value != null) {
                output.put("firstName", value);
            }
        }
        if (user.getLastName() != null) {
            String value = checkField(user.getLastName(), "^([A-Z][a-z]+(-[A-Z][a-z]+)?)|([А-Я][а-я]+(-[А-Я][а-я]+)?)$|[[:blank:]]*",
                    "Pattern.user.lastName", 0, 20, "Size.user.lastName");
            if (value != null) {
                output.put("lastName", value);
            }
        }
        if (user.getCity() != null) {
            String value = checkField(user.getCity(), null, null, 0, 128, "Size.user.city");
            if (value != null) {
                output.put("city", value);
            }
        }
        if (user.getTelNum() != null) {
            String value = checkField(user.getTelNum(), "^(\\+)?(359|0)8[789]\\d{1}(|-| )\\d{3}(|-| )\\d{3}$|[[:blank:]]*",
                    "Pattern.user.telNum", 0, 15, "Size.user.telNum");
            if (value != null) {
                output.put("telNum", value);
            }
        }

        if (!output.isEmpty() && !rawMessages) {
            transformOutputMap(output);
        }

        return output;
    }

    @Override
    public Map<String, String> checkLoginForm(UserLoginBindingModel user, boolean rawMessages) {
        Map<String, String> output = new HashMap<>();
        if (user.getEmail() != null) {
            String value = checkField(user.getEmail(), ".+@.+\\..+", "NotBlank", 0, 0, null);
            if (value != null) {
                output.put("email", value);
            }
        }
        if (user.getPassword() != null) {
            String value = checkField(user.getPassword(), null, null, 0, 0, null);
            if (value != null) {
                output.put("password", value);
            }
        }

        if (!output.isEmpty() && !rawMessages) {
            transformOutputMap(output);
        }

        return output;
    }

    @Override
    public Map<String, String> checkForgottenPasswordForm(UserEmailBindingModel user, boolean rawMessages) {
        Map<String, String> output = new HashMap<>();
        if (user.getEmail() != null) {
            String value = checkField(user.getEmail(), ".+@.+\\..+", "NotBlank", 0, 0, null);
            if (value != null) {
                output.put("email", value);
            }
        }

        if (!output.isEmpty() && !rawMessages) {
            transformOutputMap(output);
        }

        return output;
    }

    @Override
    public Map<String, String> checkUpdatePasswordForm(UserUpdatePasswordBindingModel user, boolean rawMessages) {
        Map<String, String> output = new HashMap<>();
        if (user.getConfirmPassword() != null) {
            String value = checkField(user.getConfirmPassword(), null, null, 0, 0, null);
            if (value != null) {
                output.put("confirmPassword", value);
            }
            if (user.getPassword() != null && !user.getPassword().equals(user.getConfirmPassword())) {
                output.put("confirmPassword", "confirmPassword.invalid");
            }
        }
        if (user.getPassword() != null) {
            String value = checkField(user.getPassword(), ".*[0-9].*", "password.invalid", 4, 20, "password.length");
            if (value != null) {
                output.put("password", value);
            }
            if (user.getPassword().toLowerCase().equals(user.getPassword()) ||
                    user.getPassword().toUpperCase().equals(user.getPassword())) {
                output.put("password", "password.invalid");
            }
        }

        if (!output.isEmpty() && !rawMessages) {
            transformOutputMap(output);
        }

        return output;
    }

    @Override
    public Map<String, String> checkOptionsForm(UserOptionsBindingModel user, boolean rawMessages) {
        Map<String, String> output = new HashMap<>();

        if (user.getEmail() != null) {
            String value = checkField(user.getEmail(), ".+@.+\\..+", "Email.user.email", 0, 0, null);
            if (value != null) {
                output.put("email", value);
            }
        }
        if (user.getFirstName() != null) {
            String value = checkField(user.getFirstName(), "^([A-Z][a-z]+(-[A-Z][a-z]+)?)|([А-Я][а-я]+(-[А-Я][а-я]+)?)$|[[:blank:]]*",
                    "Pattern.user.firstName", 0, 20, "Size.user.firstName");
            if (value != null) {
                output.put("firstName", value);
            }
        }
        if (user.getLastName() != null) {
            String value = checkField(user.getLastName(), "^([A-Z][a-z]+(-[A-Z][a-z]+)?)|([А-Я][а-я]+(-[А-Я][а-я]+)?)$|[[:blank:]]*",
                    "Pattern.user.lastName", 0, 20, "Size.user.lastName");
            if (value != null) {
                output.put("lastName", value);
            }
        }
        if (user.getCity() != null) {
            String value = checkField(user.getCity(), null, null, 0, 128, "Size.user.city");
            if (value != null) {
                output.put("city", value);
            }
        }
        if (user.getTelNum() != null) {
            String value = checkField(user.getTelNum(), "^(\\+)?(359|0)8[789]\\d{1}(|-| )\\d{3}(|-| )\\d{3}$|[[:blank:]]*",
                    "Pattern.user.telNum", 0, 15, "Size.user.telNum");
            if (value != null) {
                output.put("telNum", value);
            }
        }

        if (user.getPassword() != null && !user.getPassword().isEmpty()) {
            String value = checkField(user.getPassword(), ".*[0-9].*", "password.invalid", 4, 20, "password.length");
            if (value != null) {
                output.put("password", value);
            }
            if (user.getPassword().toLowerCase().equals(user.getPassword()) ||
                    user.getPassword().toUpperCase().equals(user.getPassword())) {
                output.put("password", "password.invalid");
            }
        }

        if (user.getConfirmPassword() != null && !user.getConfirmPassword().isEmpty()) {
            String value = checkField(user.getConfirmPassword(), null, null, 0, 0, null);
            if (value != null) {
                output.put("confirmPassword", value);
            }
            if (user.getPassword() != null && !user.getPassword().equals(user.getConfirmPassword())) {
                output.put("confirmPassword", "confirmPassword.invalid");
            }
        }

        if (!output.isEmpty() && !rawMessages) {
            transformOutputMap(output);
        }

        return output;
    }

    private void transformOutputMap(Map<String, String> map) {
        for (Map.Entry<String, String> e : map.entrySet()) {
            map.put(e.getKey(), messageSource.getMessage(e.getValue(), null, LocaleContextHolder.getLocale()));
        }
    }

    private String checkField(String field, String regex, String regexOutput, int min, int max, String lenghtOutput) {
        if (field.isEmpty()) {
            return "NotBlank";
        }
        if (regex != null && !field.matches(regex)) {
            return regexOutput;
        }
        if (min != 0 && max != 0) {
            if (field.length() < min || field.length() > max) {
                return lenghtOutput;
            }
        }
        return null;
    }
}
