package org.softuni.advertwall.service.impl;

import org.hibernate.exception.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.softuni.advertwall.client.EmailSenderClient;
import org.softuni.advertwall.domain.entity.UserEntity;
import org.softuni.advertwall.domain.model.binding.user.UserUpdatePasswordBindingModel;
import org.softuni.advertwall.domain.model.service.UserServiceModel;
import org.softuni.advertwall.enums.RoleType;
import org.softuni.advertwall.repository.AdvertisementRepository;
import org.softuni.advertwall.repository.RoleRepository;
import org.softuni.advertwall.repository.UserRepository;
import org.softuni.advertwall.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service("userService")
public class UserServiceImpl implements UserService, UserDetailsService {
    private final UserRepository userRepository;
    private final AdvertisementRepository advertisementRepository;
    private final RoleRepository roleRepository;
    private final EmailSenderClient emailSenderClient;

    private final ModelMapper modelMapper;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, AdvertisementRepository advertisementRepository, RoleRepository roleRepository, EmailSenderClient emailSenderClient, ModelMapper modelMapper, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.advertisementRepository = advertisementRepository;
        this.roleRepository = roleRepository;
        this.emailSenderClient = emailSenderClient;
        this.modelMapper = modelMapper;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public UserServiceModel addUser(UserServiceModel user) {
        try {
            UserEntity entity = this.modelMapper.map(user, UserEntity.class);
            entity.setRole(this.roleRepository.getFirstByRoleName(RoleType.USER));
            String token = UUID.randomUUID().toString();
            entity.setConfirmationCode(token);
            new Thread(() -> this.emailSenderClient.sendMail(user.getEmail(), "Email confirmation",
                    "Thanks for joining us " + user.getFirstName() + " " + user.getLastName() +
                            "! <br>Please confirm your registration by clicking the button below!",
                    "http://localhost:8080/users/update-password?token=" + token,
                    "Confirm Registration")).start();
            return this.modelMapper.map(this.userRepository.save(entity), UserServiceModel.class);
        } catch (DataIntegrityViolationException | ConstraintViolationException e) {
            return null;
        }
    }

    @Override
    public UserServiceModel editUser(UserServiceModel user) {
        UserEntity loggedUser = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (user.getPassword() != null) {
            user.setPassword(this.bCryptPasswordEncoder.encode(user.getPassword()));
        }

        UserEntity emailUser = this.userRepository.findByEmail(user.getEmail());
        if (emailUser != null && !emailUser.getId().equals(loggedUser.getId())){
            return null;
        }

        UserEntity u = this.userRepository.getOne(loggedUser.getId());
        this.modelMapper.map(user, u);
        loggedUser.setFirstName(user.getFirstName());
        loggedUser.setLastName(user.getLastName());

        this.userRepository.saveAndFlush(u);

        return this.modelMapper.map(u, UserServiceModel.class);
    }

    @Override
    public UserServiceModel findByEmail(String email) {
        UserEntity user = this.userRepository.findByEmail(email);
        return this.modelMapper.map(user, UserServiceModel.class);
    }

    @Override
    public List<UserServiceModel> getAllUsers() {
        return this.userRepository.findAll()
                .stream()
                .map(x -> this.modelMapper.map(x, UserServiceModel.class))
                .collect(Collectors.toList());
    }

    @Override
    public boolean isRegistered(String email) {
        return this.userRepository.findByEmail(email) != null;
    }

    @Override
    public void updatePassword(UserUpdatePasswordBindingModel model) {
        UserEntity user = this.userRepository.findFirstByConfirmationCode(model.getToken());
        user.setPassword(this.bCryptPasswordEncoder.encode(model.getPassword()));
        user.setEnabled(true);
        user.setConfirmationCode(null);

        this.userRepository.saveAndFlush(user);
    }

    @Override
    public void sendForgottenMail(String email) {
        UserEntity user = this.userRepository.findByEmail(email);
        if (user == null) {
            return;
        }
        String token = UUID.randomUUID().toString();
        user.setConfirmationCode(token);

        new Thread(() -> this.emailSenderClient.sendMail(user.getEmail(), "Password Reset",
                "Hello " + user.getFirstName() + " " + user.getLastName() +
                        "! You have requested a password reset, if you haven't - ignore this message!<br>" +
                        "To reset your password - click the button below!",
                "http://localhost:8080/users/update-password?token=" + token,
                "Reset Password")).start();

        this.userRepository.saveAndFlush(user);
    }

    @Override
    public UserServiceModel getUser(String id) {
        UserEntity user = this.userRepository.getOne(id);
        return this.modelMapper.map(user, UserServiceModel.class);
    }


    @Override
    public void updateRole(String userId, RoleType role) {
        UserEntity user = this.userRepository.getOne(userId);
        user.setRole(this.roleRepository.getFirstByRoleName(role));
        this.userRepository.saveAndFlush(user);
    }

    @Override
    public void unsubscribe(String userId) {
        UserEntity user = this.userRepository.getOne(userId);
        user.setReceiveEmails(false);
        this.userRepository.saveAndFlush(user);
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return this.userRepository.findByEmail(email);
    }
}
