package org.softuni.advertwall.service.impl;

import org.modelmapper.ModelMapper;
import org.softuni.advertwall.client.EmailSenderClient;
import org.softuni.advertwall.domain.entity.AdvertisementEntity;
import org.softuni.advertwall.domain.entity.PrivateMessageEntity;
import org.softuni.advertwall.domain.entity.UserEntity;
import org.softuni.advertwall.domain.model.service.ConversationServiceModel;
import org.softuni.advertwall.domain.model.service.MessageServiceModel;
import org.softuni.advertwall.enums.MessageStatus;
import org.softuni.advertwall.repository.AdvertisementRepository;
import org.softuni.advertwall.repository.MessageRepository;
import org.softuni.advertwall.repository.UserRepository;
import org.softuni.advertwall.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Service("messageService")
public class MessageServiceImpl implements MessageService {
    private final ModelMapper modelMapper;
    private final MessageRepository messageRepository;
    private final UserRepository userRepository;
    private final AdvertisementRepository advertisementRepository;
    private final EmailSenderClient emailClient;

    @Autowired
    public MessageServiceImpl(ModelMapper modelMapper, MessageRepository messageRepository, UserRepository userRepository, AdvertisementRepository advertisementRepository, EmailSenderClient emailClient) {
        this.modelMapper = modelMapper;
        this.messageRepository = messageRepository;
        this.userRepository = userRepository;
        this.advertisementRepository = advertisementRepository;
        this.emailClient = emailClient;
    }

    @Override
    public void sendMessage(MessageServiceModel model) {
        PrivateMessageEntity privateMessageEntity = new PrivateMessageEntity();
        UserEntity loggedUser = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        privateMessageEntity.setMessage(model.getMessage());

        UserEntity receiver = this.userRepository.getOne(model.getReceiverId());
        privateMessageEntity.setReceiver(receiver);

        AdvertisementEntity advert = this.advertisementRepository.getOne(model.getAdvertId());
        privateMessageEntity.setAdvert(advert);
        privateMessageEntity.setSender(loggedUser);

        this.messageRepository.saveAndFlush(privateMessageEntity);
        this.userRepository.setNotification(receiver, true);
        emailClient.sendMail(receiver.getEmail(), "You have a new message!",
                "You have a new message from <b>" + loggedUser.getUsername() + "</b> about your ad <b>" +
                        advert.getTitle() + "</b>. Click on the button below to read more!",
                "http://localhost:8080/messages/conversation/" + advert.getId() + "/" + loggedUser.getId(),
                "Read more"
        );
    }

    @Override
    public void readMessages(String senderId, String advertId) {
        UserEntity user = this.userRepository.getOne(senderId);
        UserEntity loggedUser = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AdvertisementEntity ad = this.advertisementRepository.getOne(advertId);

        this.messageRepository.readMessages(user, loggedUser, ad, MessageStatus.READ);
    }

    @Override
    public List<MessageServiceModel> getConversation(String userId, String advertId) {
        AdvertisementEntity advert = this.advertisementRepository.getOne(advertId);
        UserEntity loggedUser = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserEntity other = this.userRepository.getOne(userId);

        if (advert == null || advert.getId() == null || other == null || other.getId() == null) {
            return new ArrayList<>();
        }

        List<PrivateMessageEntity> allByAdvertBetweenUsers = this.messageRepository
                .findAllByAdvertBetweenUsers(advert, loggedUser, other);

        return allByAdvertBetweenUsers.stream().map(e -> {
            MessageServiceModel serviceModel = this.modelMapper
                    .map(e, MessageServiceModel.class);
            serviceModel.setOtherUsername(loggedUser.getId().equals(serviceModel.getSenderId()) ?
                    e.getReceiver().getUsername() : e.getSender().getUsername());
            serviceModel.setOtherId(loggedUser.getId().equals(serviceModel.getSenderId()) ?
                    e.getReceiver().getId() : e.getSender().getId());
            return serviceModel;
        }).collect(Collectors.toList());
    }

    @Override
    public List<ConversationServiceModel> getConversations() {
        UserEntity loggedUser = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        List<PrivateMessageEntity> conversations = this.messageRepository.findAllConversations(loggedUser);

        List<ConversationServiceModel> finalMessages = new ArrayList<>();

        loggedUser.setHasNotification(true);

        HashMap<String, String> unique = new HashMap<>();
        for (PrivateMessageEntity pm : conversations) {
            if (unique.put(pm.getAdvert().getId(), pm.getReceiver().getId()) == null) {
                ConversationServiceModel conv = this.modelMapper.map(pm, ConversationServiceModel.class);
                if (pm.getReceiver().getId().equals(loggedUser.getId())) {
                    conv.setUsername(pm.getSender().getUsername());
                    conv.setUserId(pm.getSender().getId());
                } else {
                    conv.setUsername(pm.getReceiver().getUsername());
                    conv.setUserId(pm.getReceiver().getId());
                    conv.setStatus(MessageStatus.READ);
                }
                finalMessages.add(conv);
            }
        }
        this.userRepository.setNotification(loggedUser, false);
        loggedUser.setHasNotification(false);

        return finalMessages.stream().sorted((o1, o2) -> o2.getTimestamp()
                .compareTo(o1.getTimestamp())).collect(Collectors.toList());
    }
}
