package org.softuni.advertwall.web.resource;

import org.softuni.advertwall.domain.model.binding.user.*;
import org.softuni.advertwall.service.FormValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/forms")
public class FormValidationResource {
    private final FormValidationService formValidationService;

    @Autowired
    public FormValidationResource(FormValidationService formValidationService) {
        this.formValidationService = formValidationService;
    }

    @PostMapping(value = "/check-register",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity checkRegistration(@RequestBody UserRegisterBindingModel user) {
        return ResponseEntity.ok(this.formValidationService.checkRegistrationForm(user, false));
    }

    @PostMapping(value = "/check-options",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity checkOptions(@RequestBody UserOptionsBindingModel user) {
        return ResponseEntity.ok(this.formValidationService.checkOptionsForm(user, false));
    }

    @PostMapping(value = "/check-login",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity checkLogin(@RequestBody UserLoginBindingModel user) {
        return ResponseEntity.ok(this.formValidationService.checkLoginForm(user, false));
    }

    @PostMapping(value = "/check-forgotten-password",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity fogottenPassword(@RequestBody UserEmailBindingModel user) {
        return ResponseEntity.ok(this.formValidationService.checkForgottenPasswordForm(user, false));
    }

    @PostMapping(value = "/check-update-password",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity fogottenPassword(@RequestBody UserUpdatePasswordBindingModel user) {
        return ResponseEntity.ok(this.formValidationService.checkUpdatePasswordForm(user, false));
    }

}
