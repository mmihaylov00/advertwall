package org.softuni.advertwall.web.resource;


import org.modelmapper.ModelMapper;
import org.softuni.advertwall.domain.model.binding.message.SendMessageBindingModel;
import org.softuni.advertwall.domain.model.service.MessageServiceModel;
import org.softuni.advertwall.service.MessageService;
import org.softuni.advertwall.web.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/api/messages")
public class PrivateMessageResource extends BaseController {
    private final MessageService messageService;
    private final ModelMapper modelMapper;

    @Autowired
    public PrivateMessageResource(MessageService messageService, ModelMapper modelMapper) {
        this.messageService = messageService;
        this.modelMapper = modelMapper;
    }

    @PostMapping(value = "/send",
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @PreAuthorize("isAuthenticated()")
    public ModelAndView sendMessage(SendMessageBindingModel model) {
        this.messageService.sendMessage(this.modelMapper.map(model, MessageServiceModel.class));
        return this.redirect("/messages/conversation/" + model.getReceiverId() + "/" + model.getAdvertId());
    }

}
