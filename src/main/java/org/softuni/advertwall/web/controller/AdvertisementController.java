package org.softuni.advertwall.web.controller;

import org.modelmapper.ModelMapper;
import org.softuni.advertwall.domain.entity.UserEntity;
import org.softuni.advertwall.domain.model.binding.advert.AdCreateBindingModel;
import org.softuni.advertwall.domain.model.service.AdvertServiceModel;
import org.softuni.advertwall.domain.model.view.advert.AdvertViewModel;
import org.softuni.advertwall.domain.model.view.advert.ComponentAdvertViewModel;
import org.softuni.advertwall.domain.model.view.advert.EditAdvertViewModel;
import org.softuni.advertwall.domain.model.view.category.CategoryViewModel;
import org.softuni.advertwall.enums.StatusType;
import org.softuni.advertwall.service.AdvertisementService;
import org.softuni.advertwall.service.CategoryService;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/ads")
public class AdvertisementController extends BaseController {
    private final AdvertisementService advertisementService;
    private final CategoryService categoryService;
    private final ModelMapper modelMapper;

    public AdvertisementController(AdvertisementService advertisementService, CategoryService categoryService, ModelMapper modelMapper) {
        this.advertisementService = advertisementService;
        this.categoryService = categoryService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/create")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView getCreateAd() {
        List<CategoryViewModel> categories = this.categoryService.getAllCategories().stream().map(e -> this.modelMapper
                .map(e, CategoryViewModel.class)).collect(Collectors.toList());
        AdCreateBindingModel model = new AdCreateBindingModel();
        model.setCategories(categories);
        return this.view("advert/create", "ad", model);
    }

    @GetMapping("/own")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView getOwn() {
        List<ComponentAdvertViewModel> ownModel = this.advertisementService.getOwn().stream().map(e -> {
            ComponentAdvertViewModel output = this.modelMapper.map(e, ComponentAdvertViewModel.class);
            output.setImage(e.getImages().get(0));
            return output;
        }).collect(Collectors.toList());
        return this.view("advert/own", "ads", ownModel);
    }

    @GetMapping("/view/{id}")
    public ModelAndView getView(@PathVariable("id") String id) {
        AdvertViewModel model = this.modelMapper.map(this.advertisementService.getById(id), AdvertViewModel.class);
        if (model == null){
            return this.redirect("index");
        }
        return this.view("advert/view", "ad", model);
    }


    @PostMapping("/create")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView postCreateAd(@Valid @ModelAttribute("ad") AdCreateBindingModel model, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            List<CategoryViewModel> categories = this.categoryService.getAllCategories().stream().map(e -> this.modelMapper
                    .map(e, CategoryViewModel.class)).collect(Collectors.toList());
            model.setCategories(categories);
            return this.view("advert/create", "ad", model);
        }
        UserEntity user = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        ArrayList<String> urls = this.advertisementService.upload(model.getFiles());
        AdvertServiceModel serviceModel = new AdvertServiceModel(urls, user.getId());

        this.modelMapper.map(model, serviceModel);
        String id = this.advertisementService.create(serviceModel);

        return this.redirect("/ads/view/" + id);
    }


    @GetMapping("/edit/{id}")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView getEdit(@PathVariable("id") String id) {
        if (!this.advertisementService.isOwner(id)){
            return this.redirect("/ads/own");
        }
        EditAdvertViewModel model = this.modelMapper.map(this.advertisementService.getById(id), EditAdvertViewModel.class);

        return this.view("advert/edit", "ad", model);
    }

    @GetMapping("/followed")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView getFollowed() {
        List<ComponentAdvertViewModel> followed = this.advertisementService.getFollowed().stream().map(e -> {
            ComponentAdvertViewModel output = this.modelMapper.map(e, ComponentAdvertViewModel.class);
            output.setImage(e.getImages().get(0));
            return output;
        }).collect(Collectors.toList());
        return this.view("advert/followed", "ads", followed);
    }


    @GetMapping("/update-status/{id}/{status}")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView getRepublish(@PathVariable("id") String id, @PathVariable("status") StatusType status) {
        if (!this.advertisementService.isOwner(id)){
            return this.redirect("/ads/own");
        }
        this.advertisementService.changeStatus(id, status);

        return this.redirect("/ads/view/" + id + "?updated=true");
    }


    @GetMapping(value = "/follow/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("isAuthenticated()")
    public ModelAndView getChangeFavourite(@PathVariable String id) {
        this.advertisementService.changeFavourite(id);
        return this.redirect("/ads/view/" + id + "?updated=true");
    }
}
