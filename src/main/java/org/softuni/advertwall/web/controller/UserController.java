package org.softuni.advertwall.web.controller;

import org.modelmapper.ModelMapper;
import org.softuni.advertwall.domain.entity.UserEntity;
import org.softuni.advertwall.domain.model.binding.user.*;
import org.softuni.advertwall.domain.model.service.UserServiceModel;
import org.softuni.advertwall.domain.model.view.user.ListUserViewModel;
import org.softuni.advertwall.enums.RoleType;
import org.softuni.advertwall.service.FormValidationService;
import org.softuni.advertwall.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Controller
@RequestMapping("/users")
public class UserController extends BaseController {
    private final UserService userService;
    private final FormValidationService formValidationService;

    private final ModelMapper modelMapper;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserController(UserService userService, FormValidationService formValidationService, ModelMapper modelMapper, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userService = userService;
        this.formValidationService = formValidationService;
        this.modelMapper = modelMapper;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @GetMapping(value = "/login")
    @PreAuthorize("isAnonymous()")
    public ModelAndView login() {
        return this.view("login", "user", new UserLoginBindingModel());
    }

    @GetMapping(value = "/register")
    @PreAuthorize("isAnonymous()")
    public ModelAndView register() {
        return this.view("register", "user", new UserRegisterBindingModel());
    }

    @GetMapping(value = "/unsubscribe/{userId}")
    public ModelAndView getUnsubscribe(@PathVariable String userId) {
        this.userService.unsubscribe(userId);
        return this.view("users/unsubscribe" );
    }
    @GetMapping(value = "/options")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView getOptions() {
        UserEntity user = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserOptionsBindingModel model = this.modelMapper.map(this.userService.getUser(user.getId()), UserOptionsBindingModel.class);
        model.setPassword("");
        model.setConfirmPassword("");
        return this.view("options", "user", model);
    }


    @PostMapping(value = "/options")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView postOptions(@Valid @ModelAttribute("user") UserOptionsBindingModel model, BindingResult bindingResult) {
        UserEntity userEntity = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (bindingResult.hasErrors()) {
            return this.view("options", "user", model);
        }

        Map<String, String> errors = this.formValidationService.checkOptionsForm(model, true);
        if (!errors.isEmpty()){
            errors.forEach(bindingResult::rejectValue);
            return this.view("options", "user", model);
        }

        if (model.getPassword() != null) {
            if (model.getPassword().isEmpty()){
                model.setPassword(null);
            }else {
                if (!this.bCryptPasswordEncoder.matches(model.getOldPassword(), userEntity.getPassword())) {
                    bindingResult.rejectValue("oldPassword", "password.wrong");
                    return this.view("options", "user", model);
                }
            }
        }

        UserServiceModel userServiceModel = this.userService.editUser(this.modelMapper.map(model, UserServiceModel.class));
        if (userServiceModel == null) {
            bindingResult.rejectValue("email", "email.used");
            return this.view("options", "user", model);
        }

        return this.redirect("options");
    }

    @PostMapping(value = "/register")
    @PreAuthorize("isAnonymous()")
    public ModelAndView register(@Valid @ModelAttribute("user") UserRegisterBindingModel model, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return this.view("register");
        }

        Map<String, String> errors = this.formValidationService.checkRegistrationForm(model, true);
        if (!errors.isEmpty()){
            errors.forEach(bindingResult::rejectValue);
            return this.view("register", "user", model);
        }

        UserServiceModel userServiceModel = this.userService.addUser(this.modelMapper.map(model, UserServiceModel.class));
        if (userServiceModel == null) {
            bindingResult.rejectValue("email", "email.registered");
            return this.view("register");
        }


        return this.view("register", "user", new UserRegisterBindingModel(true));
    }

    @GetMapping(value = "/forgotten-password")
    @PreAuthorize("isAnonymous()")
    public ModelAndView getForgottenPassword() {
        return this.view("forgotten-password", "email", new UserEmailBindingModel());
    }

    @PostMapping(value = "/forgotten-password")
    @PreAuthorize("isAnonymous()")
    public ModelAndView postForgottenPassword(@Valid @ModelAttribute("email") UserEmailBindingModel model) {
        this.userService.sendForgottenMail(model.getEmail());
        model.setEmail("");
        model.setSend(true);
        return this.view("forgotten-password");
    }

    @GetMapping(value = "/update-password")
    @PreAuthorize("isAnonymous()")
    public ModelAndView getUpdatePassword(@RequestParam(value = "token") String token) {
        if (token == null || token.isEmpty()) {
            return this.redirect("index");
        }
        return this.view("update-password", "user", new UserUpdatePasswordBindingModel(token));
    }

    @PostMapping(value = "/update-password")
    @PreAuthorize("isAnonymous()")
    public ModelAndView postUpdatePassword(@Valid @ModelAttribute("user") UserUpdatePasswordBindingModel model, BindingResult bindingResult) {
        boolean hasErrors = false;
        if (bindingResult.hasErrors()) {
            hasErrors = true;
        }

        Map<String, String> errors = this.formValidationService.checkUpdatePasswordForm(model, true);
        if (!errors.isEmpty()){
            errors.forEach(bindingResult::rejectValue);
            return this.view("update-password");
        }

        this.userService.updatePassword(model);

        return this.redirect("login?changed");
    }

    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('ADMINISTRATOR')")
    public ModelAndView getUsersList() {
        List<ListUserViewModel> users = this.userService.getAllUsers().stream().map(e -> this.modelMapper.map(e, ListUserViewModel.class)).collect(Collectors.toList());

        return this.view("users/list", "users", users);
    }

    @GetMapping(value = "/set-mod/{userId}")
    @PreAuthorize("hasAuthority('ADMINISTRATOR')")
    public ModelAndView getSetMod(@PathVariable String userId) {
        this.userService.updateRole(userId, RoleType.MODERATOR);
        return this.redirect("/users/list");
    }

    @GetMapping(value = "/set-user/{userId}")
    @PreAuthorize("hasAuthority('ADMINISTRATOR')")
    public ModelAndView getSetUser(@PathVariable String userId) {
        this.userService.updateRole(userId, RoleType.USER);
        return this.redirect("/users/list");
    }

    @GetMapping(value = "/set-admin/{userId}")
    @PreAuthorize("hasAuthority('ADMINISTRATOR')")
    public ModelAndView getSetAdmin(@PathVariable String userId) {
        this.userService.updateRole(userId, RoleType.ADMINISTRATOR);
        return this.redirect("/users/list");
    }
}
