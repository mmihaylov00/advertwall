package org.softuni.advertwall.web.controller;

import org.modelmapper.ModelMapper;
import org.softuni.advertwall.domain.model.view.advert.ComponentAdvertViewModel;
import org.softuni.advertwall.enums.AdvertType;
import org.softuni.advertwall.service.AdvertisementService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class HomeController extends BaseController {
    private final AdvertisementService advertisementService;
    private final ModelMapper modelMapper;

    public HomeController(AdvertisementService advertisementService, ModelMapper modelMapper) {
        this.advertisementService = advertisementService;
        this.modelMapper = modelMapper;
    }


    @GetMapping(value = {"/", "/index", "/home"})
    public ModelAndView index(@RequestParam(value = "page", defaultValue = "0", required = false) Integer page,
                              @RequestParam(value = "type", defaultValue = "selling", required = false) String type) {
        if (page < 0){
            page = 0;
        }
        AdvertType advertType = AdvertType.BUYING;
        if (type.equalsIgnoreCase("selling")) {
            advertType = AdvertType.SELLING;
        }

        HashMap<String, Object> output = new HashMap<>();

        List<ComponentAdvertViewModel> adverts = this.advertisementService.getAdverts(advertType, page)
                .stream().map(e -> {
                    ComponentAdvertViewModel ad = this.modelMapper.map(e, ComponentAdvertViewModel.class);
                    ad.setImage(e.getImages().get(0));
                    return ad;
                })
                .collect(Collectors.toList());

        output.put("adverts", adverts);
        long pages = Math.round(Math.ceil((double) this.advertisementService.getPages(advertType) / 16));

        long startPage = 0;
        if (page >= pages) {
            page = Math.toIntExact(pages-1);
        }
        if (pages > 4 && page > 2) {
            startPage = page - 2;
        }
        long endPage = page + 2;

        if (startPage == 0) {
            if (pages > 4) {
                endPage = startPage + 4;
            } else {
                endPage = startPage + pages - 1;
            }
        } else if (pages - page < 4) {
            startPage = pages - 5;
            endPage = pages - 1;
            if (endPage < 0){
                endPage = 0;
            }
        }
        output.put("page", page);
        output.put("startPage", startPage);
        output.put("endPage", endPage);
        output.put("type", type);

        return this.view("index", "output", output);
    }

}
