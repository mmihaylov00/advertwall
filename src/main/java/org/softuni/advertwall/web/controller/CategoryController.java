package org.softuni.advertwall.web.controller;

import org.modelmapper.ModelMapper;
import org.softuni.advertwall.domain.model.binding.cateogory.CategoryCreateBindingModel;
import org.softuni.advertwall.domain.model.binding.cateogory.CategoryEditBindingModel;
import org.softuni.advertwall.domain.model.service.CategoryServiceModel;
import org.softuni.advertwall.domain.model.view.category.CategoryViewModel;
import org.softuni.advertwall.service.CategoryService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/categories")
public class CategoryController extends BaseController {
    private final CategoryService categoryService;
    private final ModelMapper modelMapper;

    public CategoryController(CategoryService categoryService, ModelMapper modelMapper) {
        this.categoryService = categoryService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/list")
    @PreAuthorize("hasAuthority('ADMINISTRATOR')")
    public ModelAndView getListCategories() {
        List<CategoryViewModel> categories = this.categoryService.getAllCategories()
                .stream().map(e -> this.modelMapper.map(e, CategoryViewModel.class)).collect(Collectors.toList());
        return this.view("category/list", "categories", categories);
    }

    @GetMapping("/create")
    @PreAuthorize("hasAuthority('ADMINISTRATOR')")
    public ModelAndView getCreateCategory() {
        return this.view("category/create", "category", new CategoryCreateBindingModel());
    }

    @GetMapping("/edit/{id}")
    @PreAuthorize("hasAuthority('ADMINISTRATOR')")
    public ModelAndView getEditCategory(@PathVariable("id") String id) {
        CategoryEditBindingModel category = this.modelMapper
                .map(this.categoryService.getCategoryById(id), CategoryEditBindingModel.class);
        if (category == null){
            return redirect("/categories/list");
        }
        return this.view("category/edit", "category", category);
    }

    @PostMapping("/edit")
    @PreAuthorize("hasAuthority('ADMINISTRATOR')")
    public ModelAndView postEditCategory(@Valid @ModelAttribute("category") CategoryEditBindingModel model, BindingResult bindingResult) {
        CategoryServiceModel entity = this.categoryService
                .editCategory(this.modelMapper.map(model, CategoryServiceModel.class), model.getFile());
        if (entity == null){
            bindingResult.rejectValue("title", "category.exists");
            return this.view("category/create");
        }
        return this.redirect("/categories/list");
    }

    @PostMapping("/create")
    @PreAuthorize("hasAuthority('ADMINISTRATOR')")
    public ModelAndView postCreateCategory(@Valid @ModelAttribute("category") CategoryCreateBindingModel model, BindingResult bindingResult) {
        CategoryServiceModel category = this.categoryService
                .createCategory(this.modelMapper.map(model, CategoryServiceModel.class), model.getFile());
        if (category == null){
            bindingResult.rejectValue("title", "category.exists");
            return this.view("category/create");
        }
        return this.redirect("/categories/list");
    }

//    @PostMapping("/create")
//    @PreAuthorize("isAuthenticated()")
//    public ModelAndView postCreateAd(@Valid @ModelAttribute("ad") AdCreateBindingModel model) {
//        return this.view("category/list");
//    }
}
