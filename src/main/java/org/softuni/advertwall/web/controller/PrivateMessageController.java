package org.softuni.advertwall.web.controller;

import org.modelmapper.ModelMapper;
import org.softuni.advertwall.domain.model.view.message.ConversationsViewModel;
import org.softuni.advertwall.domain.model.view.message.MessagesViewModel;
import org.softuni.advertwall.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/messages")
public class PrivateMessageController extends BaseController {
    private final MessageService messageService;
    private final ModelMapper modelMapper;

    @Autowired
    public PrivateMessageController(MessageService messageService, ModelMapper modelMapper) {
        this.messageService = messageService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/list")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView getListMessages() {
        List<ConversationsViewModel> conversations = this.messageService.getConversations()
                .stream().map(e -> this.modelMapper.map(e, ConversationsViewModel.class)).collect(Collectors.toList());

        return this.view("messages/list", "conversations", conversations);
    }

    @GetMapping("/conversation/{userId}/{advertId}")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView getConversation(@PathVariable("userId") String userId, @PathVariable("advertId") String advertId) {
        List<MessagesViewModel> messages = this.messageService.getConversation(userId, advertId).stream()
                .map(e -> this.modelMapper.map(e, MessagesViewModel.class)).collect(Collectors.toList());
        if (messages.isEmpty()){
            return this.redirect("/messages/list");
        }
        this.messageService.readMessages(userId, advertId);
        Collections.reverse(messages);
        return this.view("messages/conversation", "messages", messages);
    }

}
