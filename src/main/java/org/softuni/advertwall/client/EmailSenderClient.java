package org.softuni.advertwall.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;

@Component
public class EmailSenderClient {
    private final JavaMailSender javaMailSender;
    private final SpringTemplateEngine templateEngine;

    @Autowired
    public EmailSenderClient(JavaMailSender javaMailSender, SpringTemplateEngine templateEngine) {
        this.javaMailSender = javaMailSender;
        this.templateEngine = templateEngine;
    }

    public void sendMail(String to, String subject, String text, String url, String button) {

        try {
            MimeMessage message = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message,
                    MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                    StandardCharsets.UTF_8.name());

            Context context = new Context();
            context.setVariable("message", text);
            context.setVariable("url", url);
            context.setVariable("button", button);
            context.setVariable("subject", subject);
            Resource resource = new ClassPathResource("/static/images/logo.png");
            context.setVariable("imageName", "logo.png");

            helper.setFrom("no-reply@advertwall.com");
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(templateEngine.process("email/basic-email-template", context), true);
            helper.addInline(resource.getFilename(), resource);

            javaMailSender.send(message);
        } catch (MessagingException ignored) {
        }
    }
}
