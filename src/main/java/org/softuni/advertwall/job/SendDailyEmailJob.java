package org.softuni.advertwall.job;

import org.softuni.advertwall.client.EmailSenderClient;
import org.softuni.advertwall.domain.model.service.UserServiceModel;
import org.softuni.advertwall.service.AdvertisementService;
import org.softuni.advertwall.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class SendDailyEmailJob {
    private final UserService userService;
    private final AdvertisementService advertisementService;
    private final EmailSenderClient emailSenderClient;

    @Autowired
    public SendDailyEmailJob(UserService userService, AdvertisementService advertisementService, EmailSenderClient emailSenderClient) {
        this.userService = userService;
        this.advertisementService = advertisementService;
        this.emailSenderClient = emailSenderClient;
    }


    @Scheduled(cron = "0 40 7 * * TUE")
//    @Scheduled(cron = "0 0 5 * * SUN")
    public void runJob() {
        System.out.println(new Date().toString());
        List<UserServiceModel> allUsers = this.userService.getAllUsers();
        new Thread(() -> {
            for (UserServiceModel user : allUsers) {
                if (user.getReceiveEmails()) {
                    if (user.getReceiveEmails()){
                        if (user.getLatestEmailTime() == null) {
                            Long views = this.advertisementService.getViewsCount(user.getId());
                            if (views > 0) {
                                this.emailSenderClient.sendMail(user.getEmail(), "Your weekly stats mail from AdvertWall",
                                        "Thanks for creating your ad in our site! You have a total of " + views + " views! " +
                                                "<br>Check back your ads by clicking the button below!" +
                                                "<br><small>If you want to unsubscribe from receiving this mail, " +
                                                "<a href='http://localhost:8080/users/unsubscribe/" + user.getId() + "'>click here</a></small>",
                                        "http://localhost:8080/ads/own", "Check out");
                                try {
                                    Thread.sleep(1000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }
            }
        }).start();

    }
}
